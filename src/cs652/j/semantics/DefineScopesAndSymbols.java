package cs652.j.semantics;

import cs652.j.parser.JBaseListener;
import cs652.j.parser.JParser;
import org.antlr.symtab.*;

public class DefineScopesAndSymbols extends JBaseListener {
	public Scope currentScope;

	public DefineScopesAndSymbols(GlobalScope globals) {
		currentScope = globals;
	}

	@Override
	public void enterFile(JParser.FileContext ctx) {
		currentScope.define((JPrimitiveType)ComputeTypes.JINT_TYPE);
		currentScope.define((JPrimitiveType)ComputeTypes.JFLOAT_TYPE);
		currentScope.define((JPrimitiveType)ComputeTypes.JSTRING_TYPE);
		currentScope.define((JPrimitiveType)ComputeTypes.JVOID_TYPE);
		ctx.scope = (GlobalScope) currentScope;
	}

	@Override
	public void exitFile(JParser.FileContext ctx) {
		currentScope = currentScope.getEnclosingScope();
	}

	@Override
	public void enterMain(JParser.MainContext ctx) {
		JMethod s = new JMethod("main", ctx);
		currentScope.define(s);
		currentScope = s;
		ctx.scope = (JMethod) currentScope;
	}

	@Override
	public void exitMain(JParser.MainContext ctx) {
		currentScope = currentScope.getEnclosingScope();
	}

	@Override
	public void enterClassDeclaration(JParser.ClassDeclarationContext ctx) {
		String className = ctx.name.getText();
		JClass s = new JClass(className, ctx);

		if(ctx.getChildCount() > 3){
			s.setSuperClass(ctx.superClass.getText());
			s.setDefNode(ctx);
			s.setEnclosingScope(currentScope);
		}

		currentScope.define(s);
		currentScope = s;
		ctx.scope = (JClass) currentScope;
	}

	@Override
	public void exitClassDeclaration(JParser.ClassDeclarationContext ctx) {
		currentScope = currentScope.getEnclosingScope();
	}

	@Override
	public void enterFieldDeclaration(JParser.FieldDeclarationContext ctx) {
		String typeName = ctx.jType().getText();
		String varName = ctx.ID().getText();

		Symbol type = currentScope.resolve(typeName);
		if ( type == null ) {
			System.err.println("No such type: " + typeName);
		}else{
			JField var = new JField(varName, ctx);
			var.setType((Type)type);
			currentScope.define(var);
		}
	}

	@Override
	public void enterLocalVariableDeclaration(JParser.LocalVariableDeclarationContext ctx) {
		String typeName = ctx.jType().getText();
		String varName = ctx.ID().getText();

		Symbol type = currentScope.resolve(typeName);
		if ( type == null ) {
			System.err.println("No such type: " + typeName);
		}else{
			JVar var = new JVar(varName, ctx);
			var.setType((Type)type);
			currentScope.define(var);
		}
	}

	@Override
	public void enterMethodDeclaration(JParser.MethodDeclarationContext ctx) {
		String methodName = ctx.ID().getText();
		Symbol type;

		if (ctx.jType() == null){
			type = (JPrimitiveType)ComputeTypes.JVOID_TYPE;
		}else {
			type = currentScope.resolve(ctx.jType().getText());
		}

		JMethod s = new JMethod(methodName, ctx);
		s.setType((Type) type);
		currentScope.define(s);
		currentScope = s;

		JArg t = new JArg("this", ctx);
		t.setType((Type) currentScope.getEnclosingScope());
		currentScope.define(t);
		ctx.scope = (JMethod) currentScope;
	}

	@Override
	public void exitMethodDeclaration(JParser.MethodDeclarationContext ctx) {
		currentScope = currentScope.getEnclosingScope();
	}

	@Override
	public void enterBlock(JParser.BlockContext ctx) {
		LocalScope s = new LocalScope(currentScope);
		currentScope.nest(s);
		currentScope = s;
		ctx.scope = (LocalScope) currentScope;
	}

	@Override
	public void exitBlock(JParser.BlockContext ctx) {
		currentScope = currentScope.getEnclosingScope();
	}

	@Override
	public void enterFormalParameter(JParser.FormalParameterContext ctx) {
		String typeName = ctx.jType().getText();
		String varName = ctx.ID().getText();

		Symbol type = currentScope.resolve(typeName);
		JArg var = new JArg(varName, ctx);
		var.setType((Type)type);
		currentScope.define(var);
	}
}