package cs652.j.semantics;

import cs652.j.parser.JBaseListener;
import cs652.j.parser.JParser;
import org.antlr.symtab.*;


public class ComputeTypes extends JBaseListener {
	public Scope currentScope;
	protected StringBuilder buf = new StringBuilder();

	public static final Type JINT_TYPE = new JPrimitiveType("int");
	public static final Type JFLOAT_TYPE = new JPrimitiveType("float");
	public static final Type JSTRING_TYPE = new JPrimitiveType("string");
	public static final Type JVOID_TYPE = new JPrimitiveType("void");

	public ComputeTypes(GlobalScope globals) {
		this.currentScope = globals;
	}

	@Override
	public void enterFile(JParser.FileContext ctx) {
		currentScope = ctx.scope;
	}

	@Override
	public void exitFile(JParser.FileContext ctx) {
		currentScope = currentScope.getEnclosingScope();
	}

	@Override
	public void enterMain(JParser.MainContext ctx) {
		currentScope = ctx.scope;
	}

	@Override
	public void exitMain(JParser.MainContext ctx) {
		currentScope = currentScope.getEnclosingScope();
	}

	@Override
	public void enterClassDeclaration(JParser.ClassDeclarationContext ctx) {
		currentScope = ctx.scope;
	}

	@Override
	public void exitClassDeclaration(JParser.ClassDeclarationContext ctx) {
		currentScope = currentScope.getEnclosingScope();
	}

	@Override
	public void enterMethodDeclaration(JParser.MethodDeclarationContext ctx) {
		currentScope = ctx.scope;
	}

	@Override
	public void exitMethodDeclaration(JParser.MethodDeclarationContext ctx) {
		currentScope = currentScope.getEnclosingScope();
	}

	@Override
	public void enterBlock(JParser.BlockContext ctx) {
		currentScope = ctx.scope;
	}

	@Override
	public void exitBlock(JParser.BlockContext ctx) {
		currentScope = currentScope.getEnclosingScope();
	}

	@Override
	public void exitIdRef(JParser.IdRefContext ctx) {
		TypedSymbol sym = (TypedSymbol) currentScope.resolve(ctx.ID().getText());
		ctx.type = sym.getType();
		buf.append(ctx.getText() + " is " + ctx.type.getName() + System.lineSeparator());
	}

	@Override
	public void exitLiteralRef(JParser.LiteralRefContext ctx) {
		if (ctx.getText().contains(".")){
			ctx.type = JFLOAT_TYPE;
		}else {
			ctx.type = JINT_TYPE;
		}
		buf.append(ctx.getText() + " is " + ctx.type.getName() + System.lineSeparator());
	}

	@Override
	public void exitCtorCall(JParser.CtorCallContext ctx) {
		String type = currentScope.resolve(ctx.ID().getText()).getName();
		ctx.type = (Type) currentScope.resolve(type);
		buf.append(ctx.getText() + " is " + ctx.type.getName() + System.lineSeparator());
	}

	@Override
	public void exitFieldRef(JParser.FieldRefContext ctx) {
		JClass theClass = (JClass) currentScope.resolve(ctx.expression().type.getName());
		TypedSymbol sym = (TypedSymbol) theClass.resolveMember(ctx.ID().getText());
		ctx.type = sym.getType();

		buf.append(ctx.getText() + " is " + ctx.type.getName() + System.lineSeparator());
	}

	@Override
	public void exitQMethodCall(JParser.QMethodCallContext ctx) {
		JClass theClass = (JClass) currentScope.resolve(ctx.expression().type.getName());
		TypedSymbol sym = (TypedSymbol) theClass.resolveMember(ctx.ID().getText());
		ctx.type = sym.getType();

		buf.append(ctx.getText() + " is " + ctx.type.getName() + System.lineSeparator());
	}


	@Override
	public void exitMethodCall(JParser.MethodCallContext ctx) {
		TypedSymbol sym = (TypedSymbol) currentScope.resolve(ctx.ID().getText());
		ctx.type = sym.getType();
		buf.append(ctx.getText() + " is " + ctx.type.getName() + System.lineSeparator());
	}

	@Override
	public void exitThisRef(JParser.ThisRefContext ctx) {
		TypedSymbol sym = (TypedSymbol) currentScope.resolve(ctx.getText());
		ctx.type = sym.getType();
		buf.append(ctx.getText() + " is " + ctx.type.getName() + System.lineSeparator());
	}

	public String getRefOutput() {
		return buf.toString();
	}
}