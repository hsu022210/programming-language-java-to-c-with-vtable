package cs652.j.semantics;

import org.antlr.symtab.FieldSymbol;
import org.antlr.v4.runtime.ParserRuleContext;

public class JField extends FieldSymbol {
    public JField(String name, ParserRuleContext tree) {
        super(name);
        setDefNode(tree);
    }
}