package cs652.j.semantics;

import org.antlr.symtab.ParameterSymbol;
import org.antlr.v4.runtime.ParserRuleContext;

public class JArg extends ParameterSymbol {
    public JArg(String name, ParserRuleContext tree) {
        super(name);
        setDefNode(tree);
    }
}