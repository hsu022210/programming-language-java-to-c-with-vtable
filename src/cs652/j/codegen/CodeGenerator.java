package cs652.j.codegen;

import cs652.j.codegen.model.*;
import cs652.j.parser.JBaseVisitor;
import cs652.j.parser.JParser;
import cs652.j.semantics.JClass;
import cs652.j.semantics.JField;
import cs652.j.semantics.JMethod;
import cs652.j.semantics.JPrimitiveType;
import org.antlr.symtab.*;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupFile;

import java.util.Comparator;

public class CodeGenerator extends JBaseVisitor<OutputModelObject> {
	public STGroup templates;
	public String fileName;

	public Scope currentScope;
	public JClass currentClass;

	public CodeGenerator(String fileName) {
		this.fileName = fileName;
		templates = new STGroupFile("cs652/j/templates/C.stg");
	}

	public CFile generate(ParserRuleContext tree) {
		return (CFile)visit(tree);
	}

	@Override
	public OutputModelObject visitFile(JParser.FileContext ctx) {
		currentScope = ctx.scope;
		CFile file = new CFile(fileName);
		file.main = (MainMethod) visit(ctx.main());
		for (JParser.ClassDeclarationContext clazz: ctx.classDeclaration()){
			file.add((ClassDef) visit(clazz));
		}
		return file;
	}

	@Override
	public OutputModelObject visitClassDeclaration(JParser.ClassDeclarationContext ctx) {
		JClass JClazz = ctx.scope;
		currentClass = ctx.scope;
		ClassDef clazz = new ClassDef(JClazz);

		for (MethodSymbol method : JClazz.getMethods()) {
			FuncName funcName = new FuncName((JMethod) method);
			funcName.slotNumber = funcName.method.getSlotNumber();

			ClassSymbol superClassScope = JClazz.getSuperClassScope();

			while (superClassScope != null){
				if (superClassScope.resolveMember(method.getName()) == null){
					funcName.slotNumber += superClassScope.getNumberOfDefinedMethods();
				}
				superClassScope = superClassScope.getSuperClassScope();
			}
			clazz.vtable.add(funcName);
		}

		clazz.vtable.sort(new Comparator<FuncName>() {
			@Override
			public int compare(FuncName o1, FuncName o2) {
				return (o1.slotNumber - o2.slotNumber);
			}
		});

		for (FieldSymbol field : JClazz.getFields()) {
			TypeSpec type;
			String fieldType = field.getType().getName();
			if (fieldType.equals("int") || fieldType.equals("float")){
				type = new PrimitiveTypeSpec(fieldType);
			} else {
				type = new ObjectTypeSpec(fieldType);
			}
			VarDef varDef = new VarDef(type, field.getName());
			clazz.fields.add(varDef);
		}

		for (ParseTree child : ctx.classBody().children) {
			OutputModelObject m = visit(child);

			if (m instanceof MethodDef) {
				clazz.methods.add((MethodDef) m);
			}
		}
		return clazz;
	}

	@Override
	public OutputModelObject visitClassBodyDeclaration(JParser.ClassBodyDeclarationContext ctx) {
		return visit(ctx.getChild(0));
	}

	@Override
	public OutputModelObject visitFieldDeclaration(JParser.FieldDeclarationContext ctx) {
		TypeSpec type;
		if (ctx.jType().ID()!= null){
			type = new ObjectTypeSpec(ctx.jType().ID().getText());
		} else {
			type = new PrimitiveTypeSpec(ctx.jType().getText());
		}
		return new VarDef(type, ctx.ID().getText());
	}

	@Override
	public OutputModelObject visitMethodDeclaration(JParser.MethodDeclarationContext ctx) {
		currentScope = ctx.scope;
		JMethod jMethod = ctx.scope;
		MethodDef method = new MethodDef(jMethod);

		if (ctx.formalParameters().formalParameterList() != null){
			for (ParseTree child : ctx.formalParameters().formalParameterList().children) {
				OutputModelObject m = visit(child);
				method.args.add((VarDef) m);
			}
		}

		if (ctx.jType() != null){
			method.returnType = (TypeSpec) visit(ctx.jType());
		} else {
			method.returnType = new PrimitiveTypeSpec(jMethod.getType().getName());
		}
		method.body = (Block) visit(ctx.methodBody());
		return method;
	}

	@Override
	public OutputModelObject visitJType(JParser.JTypeContext ctx) {
		TypeSpec type;
		if (ctx.ID() != null){
			type = new ObjectTypeSpec(ctx.ID().getText());
		}else {
			type = new PrimitiveTypeSpec(ctx.getText());
		}
		return type;
	}

	@Override
	public OutputModelObject visitFormalParameter(JParser.FormalParameterContext ctx) {
		TypeSpec type = (TypeSpec) visit(ctx.jType());
		return new VarDef(type, ctx.ID().getText());
	}

	@Override
	public OutputModelObject visitMain(JParser.MainContext ctx) {
		currentScope = ctx.scope;
		JMethod jMethod = ctx.scope;
		MainMethod method = new MainMethod(jMethod);
		method.body = (Block) visit(ctx.block());
		return method;
	}

	@Override
	public OutputModelObject visitMethodBody(JParser.MethodBodyContext ctx) {
		return visit(ctx.block());
	}

	@Override
	public OutputModelObject visitBlock(JParser.BlockContext ctx) {
		currentScope = ctx.scope;
		Block block = new Block();
		for (JParser.StatementContext stat: ctx.statement()){
			OutputModelObject omo = visit(stat);
			if (omo instanceof Stat){
				block.instrs.add((Stat) omo);
			} else if (omo instanceof VarDef){
				block.locals.add((VarDef) omo);
			}
		}
		return block;
	}

	@Override
	public OutputModelObject visitLocalVarStat(JParser.LocalVarStatContext ctx) {
		return visit(ctx.localVariableDeclaration());
	}

	@Override
	public OutputModelObject visitLocalVariableDeclaration(JParser.LocalVariableDeclarationContext ctx) {
		TypeSpec type = (TypeSpec) visit(ctx.jType());
		return new VarDef(type, ctx.ID().getText());
	}

	@Override
	public OutputModelObject visitReturnStat(JParser.ReturnStatContext ctx) {
		return new ReturnStat((Expr) visit(ctx.expression()));
	}

	@Override
	public OutputModelObject visitPrintStat(JParser.PrintStatContext ctx) {
		PrintStat stat = new PrintStat(ctx.STRING().getText());
		for (ParseTree child : ctx.expressionList().children) {
			OutputModelObject m = visit(child);
			stat.args.add((Expr) m);
		}
		return stat;
	}

	@Override
	public OutputModelObject visitPrintStringStat(JParser.PrintStringStatContext ctx) {
		return new PrintStringStat(ctx.STRING().getText());
	}

	@Override
	public OutputModelObject visitWhileStat(JParser.WhileStatContext ctx) {
		return new WhileStat((Expr) visit(ctx.parExpression()), (Block) visit(ctx.statement()));
	}

	@Override
	public OutputModelObject visitBlockStat(JParser.BlockStatContext ctx) {
		return visit(ctx.block());
	}

	@Override
	public OutputModelObject visitIfStat(JParser.IfStatContext ctx) {
		Stat stat;
		if (ctx.getChildCount()>3){
			stat = new IfElseStat((Expr) visit(ctx.parExpression()), (Stat) visit(ctx.statement(0)), (Stat) visit(ctx.statement(1)));
		} else {
			stat = new IfStat((Expr) visit(ctx.parExpression()), (Stat) visit(ctx.statement(0)));
		}
		return stat;
	}

	@Override
	public OutputModelObject visitParExpression(JParser.ParExpressionContext ctx) {
		return visit(ctx.expression());
	}

	@Override
	public OutputModelObject visitAssignStat(JParser.AssignStatContext ctx) {
		Expr left = (Expr) visit(ctx.expression(0));
		Expr right = (Expr) visit(ctx.expression(1));
		AssignStat stat = new AssignStat(left, right);
		Type leftType = ctx.expression(0).type;
		Type rightType = ctx.expression(1).type;

		if(leftType != rightType){
			if(!(leftType instanceof JPrimitiveType) ){
				stat.typeCast = new TypeCast(right, new ObjectTypeSpec(leftType.getName()));
			}
		}
		return stat;
	}

	@Override
	public OutputModelObject visitCallStat(JParser.CallStatContext ctx) {
		return new CallStat((Expr)visit(ctx.expression()));
	}

// my method is about 12 lines because I factored out getTypeSpec() and getArgs()
// further this method is complete cut-and-paste on the regular method call. factors of these methods are much smaller
	@Override
	public OutputModelObject visitQMethodCall(JParser.QMethodCallContext ctx) {
		MethodCall methodCall = new MethodCall(ctx.ID().getText());

		Expr receiver = (Expr) visit(ctx.expression());

		JClass clazz = (JClass) currentScope.resolve(ctx.expression().type.getName());
		JMethod jMethod = (JMethod) clazz.resolveMember(ctx.ID().getText());
		JClass fptrClass = (JClass) jMethod.getEnclosingScope();
		ObjectTypeSpec recClassType = new ObjectTypeSpec(fptrClass.getName());

		TypeSpec returnType;

		if (jMethod.getType() instanceof JPrimitiveType){
			 returnType = new PrimitiveTypeSpec(jMethod.getType().getName());
		}else{
			returnType = new ObjectTypeSpec(jMethod.getType().getName());
		}

		FuncPtrType fpt = new FuncPtrType(returnType, fptrClass);

		TypeCast cast = new TypeCast(receiver,recClassType);

		TypeSpec recType;

// why are you testing what the receiver is? Why did we do the compute types pass if you are not using?
		if (receiver instanceof VarRef){
			VarRef r = (VarRef) receiver;
			recType = r.type;
		} else if (receiver instanceof ThisRef){
			ThisRef r = (ThisRef) receiver;
			recType = r.type;
		} else if (receiver instanceof FieldRef){
			FieldRef r = (FieldRef) receiver;
			recType = r.object.type;
		} else {
			recType = recClassType;
		}

		methodCall.args.add(cast);
		fpt.argTypes.add(cast.type);

		if(ctx.expressionList()!=null){
			for(ParseTree expr : ctx.expressionList().expression()){
				OutputModelObject omo = visit(expr);
				TypeCast typeCast;
// you should not be parsing again here. don't keep checking the model object that comes back
				if(omo instanceof LiteralRef){
					typeCast = new TypeCast((Expr) omo,null);
					fpt.argTypes.add(((LiteralRef) omo).type);
					methodCall.args.add(typeCast);
				}
				else if(omo instanceof VarRef){
					typeCast = new TypeCast(((VarRef) omo), ((VarRef) omo).type);
					fpt.argTypes.add(((VarRef) omo).type);
					methodCall.args.add(typeCast);
				}
				else if(omo instanceof CtorCall){
					typeCast = new TypeCast((CtorCall) omo, null);
					fpt.argTypes.add(((CtorCall) omo).type);
					methodCall.args.add(typeCast);
				}
				else if (omo instanceof FieldRef)  {
					typeCast = new TypeCast(((FieldRef) omo).object, ((FieldRef) omo).type);
					fpt.argTypes.add(((FieldRef) omo).type);
					methodCall.args.add(typeCast);
				}
			}
		}
		methodCall.fptrType = fpt;
		methodCall.receiver = receiver;
		methodCall.receiverType = recType;

		return methodCall;
	}

// share code with the above function to make this much smaller
	@Override
	public OutputModelObject visitMethodCall(JParser.MethodCallContext ctx) {
		String methodName = ctx.ID().getText();
		MethodCall methodCall = new MethodCall(methodName);

		JClass jClass = (JClass) currentScope.resolve(currentClass.getName());
		JMethod jMethod = (JMethod) jClass.resolveMember(methodName);
		JClass superClass = (JClass) jMethod.getEnclosingScope();

		TypeSpec superClassType = new ObjectTypeSpec(superClass.getName());

		ObjectTypeSpec type = new ObjectTypeSpec(jClass.getName());
		Expr receiver = new ThisRef(type, "this");

		TypeSpec returnType;

		if (jMethod.getType() instanceof JPrimitiveType){
			returnType = new PrimitiveTypeSpec(jMethod.getType().getName());
		}else{
			returnType = new ObjectTypeSpec(jMethod.getType().getName());
		}

		FuncPtrType fpt = new FuncPtrType(returnType, superClass);
		TypeCast cast = new TypeCast(receiver,superClassType);

		methodCall.args.add(cast);
		fpt.argTypes.add(cast.type);

		if(ctx.expressionList()!=null){
			for(ParseTree expr : ctx.expressionList().expression()){
				OutputModelObject omo = visit(expr);
				TypeCast typeCast;
				if(omo instanceof LiteralRef){
					typeCast = new TypeCast((Expr) omo,null);
					fpt.argTypes.add(((LiteralRef) omo).type);
					methodCall.args.add(typeCast);
				}
				else if(omo instanceof VarRef){
					typeCast = new TypeCast(((VarRef) omo),((VarRef) omo).type);
					fpt.argTypes.add(((VarRef) omo).type);
					methodCall.args.add(typeCast);
				}
				else if(omo instanceof CtorCall){
					TypeSpec ctorType = new ObjectTypeSpec(((CtorCall) omo).name);
					typeCast = new TypeCast((CtorCall) omo, ctorType);
					fpt.argTypes.add(((CtorCall) omo).type);
					methodCall.args.add(typeCast);
				}
			}
		}
		methodCall.fptrType = fpt;
		methodCall.receiver = receiver;
		methodCall.receiverType = type;

		return methodCall;
	}

	@Override
	public OutputModelObject visitIdRef(JParser.IdRefContext ctx) {
		String id = ctx.ID().getText();
		TypedSymbol sym = (TypedSymbol) currentScope.resolve(id);

		if (sym instanceof JField)   {
			id = "this->" + id;
		}
		TypeSpec typeSpec;
		String typeName = sym.getType().getName();

		if (typeName.equals("int") || typeName.equals("float")) {
			typeSpec = new PrimitiveTypeSpec(typeName);
		} else  {
			typeSpec = new ObjectTypeSpec(typeName);
		}
		return new VarRef(typeSpec, id);
	}

	@Override
	public OutputModelObject visitThisRef(JParser.ThisRefContext ctx) {
		ObjectTypeSpec type = new ObjectTypeSpec(ctx.type.getName());
		return new ThisRef(type, "this");
	}

	@Override
	public OutputModelObject visitLiteralRef(JParser.LiteralRefContext ctx) {
		TypeSpec typeSpec = new PrimitiveTypeSpec(ctx.type.getName());
		return new LiteralRef(typeSpec, ctx.getText());
	}

	@Override
	public OutputModelObject visitFieldRef(JParser.FieldRefContext ctx) {
		JClass jClass = (JClass) currentScope.resolve(ctx.expression().type.getName());
		TypedSymbol symbol = (TypedSymbol)jClass.resolveMember(ctx.ID().getText());
		TypeSpec type;

		if (Character.isUpperCase(symbol.getType().getName().charAt(0))){
			type = new ObjectTypeSpec(symbol.getType().getName());
		} else {
			type = new PrimitiveTypeSpec(symbol.getType().getName());
		}

		Expr expr = (Expr) visit(ctx.expression());
		return new FieldRef(ctx.ID().getText(), expr, type);
	}

	@Override
	public OutputModelObject visitCtorCall(JParser.CtorCallContext ctx) {
		TypeSpec typeSpec = new ObjectTypeSpec(ctx.type.getName());
		return new CtorCall(typeSpec, ctx.ID().getText());
	}
}
