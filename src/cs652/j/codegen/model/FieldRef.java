package cs652.j.codegen.model;

/**
 * Created by hsu022210 on 4/4/17.
 */
public class FieldRef extends Expr {
    public String id;
    public TypeSpec type;
    @ModelElement public Expr object;

    public FieldRef(String id, Expr object, TypeSpec type){
        this.id = id;
        this.object = object;
        this.type = type;
    }
}
