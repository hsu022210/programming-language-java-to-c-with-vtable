package cs652.j.codegen.model;

import cs652.j.semantics.JMethod;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hsu022210 on 3/31/17.
 */
public class MainMethod extends MethodDef {
    public MainMethod(JMethod method) {
        super(method);
    }
}
