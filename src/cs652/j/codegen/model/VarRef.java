package cs652.j.codegen.model;

/**
 * Created by hsu022210 on 4/4/17.
 */
public class VarRef extends Expr {
    public String id;
    public TypeSpec type;

    public VarRef(TypeSpec type, String id){
        this.type = type;
        this.id = id;
    }
}
