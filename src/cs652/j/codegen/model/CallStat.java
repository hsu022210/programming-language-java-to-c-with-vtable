package cs652.j.codegen.model;

/**
 * Created by hsu022210 on 4/4/17.
 */
public class CallStat extends Stat {
    @ModelElement public Expr call;

    public CallStat(Expr call){
        this.call = call;
    }
}
