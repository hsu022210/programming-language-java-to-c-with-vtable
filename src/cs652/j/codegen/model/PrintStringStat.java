package cs652.j.codegen.model;

/**
 * Created by hsu022210 on 4/4/17.
 */
public class PrintStringStat extends Stat {
    public String string;

    public PrintStringStat (String string){
        this.string = string;
    }
}
