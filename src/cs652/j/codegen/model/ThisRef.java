package cs652.j.codegen.model;

/**
 * Created by hsu022210 on 4/4/17.
 */
public class ThisRef extends Expr {
    public TypeSpec type;
    public String id;

    public ThisRef(TypeSpec type, String id)  {
        this.type = type;
        this.id = id;
    }
}
