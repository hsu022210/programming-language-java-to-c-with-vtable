package cs652.j.codegen.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hsu022210 on 4/4/17.
 */
public class PrintStat extends Stat {
    @ModelElement public List<Expr> args = new ArrayList<>();

    public String string;

    public PrintStat (String string){
        this.string = string;
    }
}
