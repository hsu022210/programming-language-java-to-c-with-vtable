package cs652.j.codegen.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hsu022210 on 4/4/17.
 */
public class MethodCall extends Expr {
    public String name;

    @ModelElement public TypeSpec receiverType;

    @ModelElement public Expr receiver;

    @ModelElement public List<Expr> args = new ArrayList<>();

    @ModelElement public FuncPtrType fptrType;

    public MethodCall (String name) {
        this.name = name;
    }
}
