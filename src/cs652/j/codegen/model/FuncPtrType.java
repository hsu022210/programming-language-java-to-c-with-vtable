package cs652.j.codegen.model;

import cs652.j.semantics.JClass;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hsu022210 on 4/6/17.
 */
public class FuncPtrType extends OutputModelObject {
    @ModelElement public TypeSpec returnType;

    @ModelElement public List<TypeSpec> argTypes = new ArrayList<>();

    public JClass classType;

    public FuncPtrType(TypeSpec returnType, JClass clazz) {
        this.returnType = returnType;
        this.classType = clazz;
    }
}
