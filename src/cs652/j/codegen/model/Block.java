package cs652.j.codegen.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hsu022210 on 3/31/17.
 */
public class Block extends OutputModelObject  {
    @ModelElement public List<VarDef> locals = new ArrayList<>();
    @ModelElement public List<Stat> instrs = new ArrayList<>();
}
