package cs652.j.codegen.model;

import cs652.j.semantics.JMethod;
import cs652.j.semantics.JPrimitiveType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hsu022210 on 4/4/17.
 */

//MethodDef(m,funcName,returnType,args,body)

public class MethodDef extends OutputModelObject {
    private JMethod method;
    @ModelElement public FuncName funcName;
    @ModelElement public TypeSpec returnType;
    @ModelElement public Block body = new Block();
    @ModelElement public List<VarDef> args = new ArrayList<>();

    public MethodDef(JMethod method){
        this.method = method;
        this.funcName = new FuncName(method);
    }

    public String getName(){
        return method.getName();
    }
}
