package cs652.j.codegen.model;

/**
 * Created by hsu022210 on 3/31/17.
 */
public class VarDef extends OutputModelObject  {
    @ModelElement public TypeSpec type;
    public String id;

    public VarDef(TypeSpec type, String id){
        this.type = type;
        this.id = id;
    }
}
