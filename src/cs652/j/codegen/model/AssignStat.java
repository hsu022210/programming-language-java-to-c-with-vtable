package cs652.j.codegen.model;

/**
 * Created by hsu022210 on 4/4/17.
 */
public class AssignStat extends Stat{
    @ModelElement public Expr left;
    @ModelElement public Expr right;
    public TypeCast typeCast;

    public AssignStat(Expr left, Expr right) {
        this.left = left;
        this.right = right;
    }
}
