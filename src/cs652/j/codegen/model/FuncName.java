package cs652.j.codegen.model;

import cs652.j.semantics.JMethod;

/**
 * Created by hsu022210 on 4/4/17.
 */
public class FuncName extends OutputModelObject {
    public JMethod method;
    public int slotNumber;

    public FuncName (JMethod method){
        this.method = method;
    }

    public String getClassName(){
        return method.getEnclosingScope().getName();
    }

    public String getMethodName(){
        return method.getName();
    }
}
