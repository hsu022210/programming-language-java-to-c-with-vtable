package cs652.j.codegen.model;

/**
 * Created by hsu022210 on 4/4/17.
 */
public class IfStat extends Stat {
    @ModelElement public Expr condition;
    @ModelElement public Stat stat;

    public IfStat(Expr condition, Stat stat){
        this.condition = condition;
        this.stat = stat;
    }
}
