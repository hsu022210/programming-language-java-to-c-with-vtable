package cs652.j.codegen.model;

/**
 * Created by hsu022210 on 4/4/17.
 */
public class IfElseStat extends IfStat {
    @ModelElement public Stat elseStat;

    public IfElseStat(Expr condition, Stat stat, Stat elseStat) {
        super(condition, stat);
        this.elseStat = elseStat;
    }
}
