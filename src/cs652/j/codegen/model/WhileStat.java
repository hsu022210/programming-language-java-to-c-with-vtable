package cs652.j.codegen.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hsu022210 on 4/4/17.
 */
public class WhileStat extends Stat {
    @ModelElement public Expr condition;
    @ModelElement public Block stat;

    public WhileStat(Expr condition, Block stat){
        this.condition = condition;
        this.stat = stat;
    }
}
