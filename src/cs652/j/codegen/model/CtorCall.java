package cs652.j.codegen.model;

/**
 * Created by hsu022210 on 4/4/17.
 */
public class CtorCall extends Expr {
    public TypeSpec type;
    public String name;

    public CtorCall(TypeSpec type, String name){
        this.type = type;
        this.name = name;
    }
}
