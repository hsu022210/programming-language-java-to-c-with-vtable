package cs652.j.codegen.model;

/**
 * Created by hsu022210 on 4/4/17.
 */
public class ReturnStat extends Stat {
    @ModelElement public Expr expr;

    public ReturnStat(Expr expr){
        this.expr = expr;
    }
}
