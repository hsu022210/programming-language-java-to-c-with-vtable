package cs652.j.codegen.model;

import cs652.j.semantics.JClass;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hsu022210 on 3/31/17.
 */
public class ClassDef extends OutputModelObject  {
    public JClass clazz;
    public String className;
    @ModelElement public List<VarDef> fields = new ArrayList<>();
    @ModelElement public List<MethodDef> methods = new ArrayList<>();
    @ModelElement public List<FuncName> vtable = new ArrayList<>();

    public ClassDef(JClass clazz){
        this.clazz = clazz;
    }

    public String getName(){
        return clazz.getName();
    }
}
